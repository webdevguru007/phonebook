<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Contact extends Model
{
    use HasFactory;

    protected $table = 'contacts';
    protected $fillable = ['name', 'phone'];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'contacts_users', 'contact_id', 'user_id');
    }
}
