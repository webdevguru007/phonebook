<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    /**
     * @var int
     */
    protected $indexPerPage = 15;

    /**
     * @var int
     */
    protected $favoritePerPage = 15;

    /**
     * @return View
     */
    public function index(): View
    {
        $contacts = Contact::orderby('id', 'desc')->with('users')->paginate($this->indexPerPage);

        return view('index', compact('contacts'));
    }

    /**
     * @return View
     */
    public function favorite(): View
    {
        $contacts = Contact::orderby('id', 'desc')->whereHas('users', function ($query){
            $query->where('user_id', Auth::id());
        })->paginate($this->favoritePerPage);

        return view('favorite', compact('contacts'));
    }

    /**
     * @param Contact $id
     * @return RedirectResponse
     */
    public function switchFavorite(Contact $id): RedirectResponse
    {
        $contact = Contact::findOrFail($id);
        $contact->users()->toggle([Auth::id()]);

        return redirect()->back();
    }
}
