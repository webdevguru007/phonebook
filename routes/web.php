<?php

use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware(['auth:sanctum',config('jetstream.auth_session'),'verified'])->group(function () {
    Route::get('/', [IndexController::class, 'index'])->name('dashboard');
    Route::get('/favorite', [IndexController::class, 'favorite'])->name('favorite');
    Route::get('/switch-favorite/{id}', [IndexController::class, 'switchFavorite'])->name('switchFavorite');
    Route::redirect('/dashboard', '/');
});
